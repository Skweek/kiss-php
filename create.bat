@echo off
	set /p id="Enter Project name:"
    	set "source=%~dp0src"
    	set "target=%~dp0%id%" 

	REM CREATE FOLDER STRUCTURE
	xcopy /t /e /i "%source%" "%target%"
	
	setlocal ENABLEDELAYEDEXPANSION
	REM CREATE SYMLINKS
	cd %source%
	for /R %%f in (*.*) do (		
		set str=%%f
		set "str=!str:%source%=%target%!"
		echo %%f
		echo !str!
		mklink "!str!" "%%f"
	)
	endlocal
pause
    	