<?php

session_start();
require_once("classes/helpers/config.php");

function autoload_class($class_name) 
{
    $directories = array(
        //Extras
        'classes/helpers/',
        'classes/objects/',

        // Controllers
        'classes/controllers/',
        'classes/controllers/tools/',
        'classes/controllers/base/',

        // Models
        'classes/models/',
        'classes/models/base/',
    );
    foreach ($directories as $directory) 
	{
        $filename = $directory . $class_name . '.php';
        if (is_file($filename)) 
		{
            require($filename);
            break;
        }
    }
}
spl_autoload_register('autoload_class');

// Redirect if we have an encoded slash in the url
$actual_link = "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if($actual_link != urldecode($actual_link))
{
    header("Location:" . urldecode($actual_link));
}

$request_data = "";
if(isset($_SERVER['PATH_INFO'])) $request_data = $_SERVER['PATH_INFO'];

$request = new Request($request_data);

if (class_exists($request->controller_name)) 
{
	$controller = new $request->controller_name($request);
    $action_name = strtolower($request->method);
    $data = $request->url_elements[2];
    
    if(!method_exists($controller, $action_name))
    {
        $action_name = "default";
        $data = $request->url_elements[1];
    }
    call_user_func(array($controller, $action_name), $data);

    // Render response
    $controller->render();

}
else
{
	header('HTTP/1.1 404 Not Found');
}