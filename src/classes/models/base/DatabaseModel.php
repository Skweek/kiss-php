<?php

class DatabaseModel
{
    private $connection;
    private $lastQuery;
    private $lastResult;

    private $host;
    private $user;
    private $passwd;
    private $database_name;

    private $table;

    private function connect()
    {
        $opt = array(
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
        );
        $dsn = "mysql:host=$this->host;dbname=$this->database_name";

        try
		{
			$this->connection = new PDO($dsn, $this->user, $this->passwd, $opt);
		}
		catch (PDOException $ex)
		{
			echo $ex->getMessage();
		}
    } 
    
    public function __construct($table)
    {
        // Get details from config
		$this->host = config::Key("db_host");
		$this->database_name = config::Key("db_name");
		$this->user = config::Key("db_user");
        $this->passwd = config::Key("db_passwd");
        $this->table = $table;
        $this->connect();

        return $this;
    }

    public function GetRow($row_num)
    {
        if($this->lastResult == null) return null;
        if(count($this->lastResult) < $row_num) return null;

        return $this->lastResult[$row_num];
    }
    public function GetRows()
    {
        if($this->lastResult == null) return null;

        return $this->lastResult;
    }

    private function query($query, $data)
    {
        try
		{
            $this->lastQuery = $this->debugQuery($query, $data);
            $stmt = $this->connection->prepare($query);
			$stmt->execute($data);
            $this->lastResult = $stmt->fetchAll();
		}
		catch (PDOException $ex)
		{
            echo $ex->getMessage();
            echo "<pre>$this->lastQuery</pre>";
		}
    }

    private function debugQuery($query, $data)
    {
        $debugSQL = $query;
        foreach($data as $key => $value)
        {
            $debugSQL = str_replace("$key,", "$value,", $debugSQL);
        }
        return $debugSQL;
    }

    public function select($fields, $where = "1=1")
    {
        if($this->connection == null) return null;
        
        $query = "SELECT $fields FROM $this->table WHERE $where;";
        $this->query($query, null);

        return $this;
    }
}