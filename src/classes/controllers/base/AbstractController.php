<?php

abstract class AbstractController 
{
    protected $request;
    protected $data = array();
    protected $view;
    private $root_url;
    private $view_base = "classes/views/";
    private $base_html = "base.html";

    public function __construct($request)
    {
        $this->request = $request;
        $this->root_url = (!empty($_SERVER["HTTPS"]) ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"];
    }
    public function default()
    {
        throw new Exception("Unimplemented default function in " . get_class($this));
    }
    public function render()
    {
        switch($this->request->controller_reponse)
        {
            case "json":
                header("Content-Type: application/json");
                echo json_encode($this->data);
            break;
            case "xml":
                $xml_data = new SimpleXMLElement("<?xml version=\"1.0\"?><data></data>");
                $this->array_to_xml($this->data, $xml_data);
                header('Content-type: text/xml');
                echo $xml_data->asXML();
            break;

            case "html":
            default:
                $html = file_get_contents($this->view_base . $this->base_html);
                // Replace {{content with the view page}}
                $template = Template::render(file_get_contents($this->view_base . $this->view), $this->data);
                $html = str_replace("{{content}}", $template, $html);
                // Replace {{root}} with the actual domain to point at things correctly
                $html = str_replace("{{root}}", $this->root_url, $html);
                echo $html;
            break;
        }
    }

    private function array_to_xml($data, &$xml_data) 
    {
        // https://stackoverflow.com/questions/1397036/how-to-convert-array-to-simplexml
        foreach($data as $key => $value) 
        {
            if(is_numeric($key))
            {
                $key = "item$key"; //dealing with <0/>..<n/> issues
            }
            if(is_array($value)) 
            {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } 
            else
            {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
         }
    }
}