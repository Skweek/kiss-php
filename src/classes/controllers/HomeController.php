<?php

class HomeController extends AbstractController
{

    public function default()
    {
        $this->view = "home.html";
        $this->data["test"] = $this->request->url_elements[1];
    }
}