<?php 

class Template
{
    public static $regex = "/\{\{(.*?)\}\}/";
    public static function Render($template, $data)
    {
        // replace the root tag first.
        $root_url = (!empty($_SERVER["HTTPS"]) ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"];
        if(isset($data["root"])) $root_url = $data["root"];
        $template = str_replace("{{root}}", $root_url, $template);

        // Get all items from template
        preg_match_all(self::$regex, $template, $items, PREG_OFFSET_CAPTURE);

        foreach($items[0] as $match)
        {
            // Store some variables
            $original = $match[0];
            $tag = str_replace("{{", "", str_replace("}}", "", $original));
                
            // Handle the tag
            if(substr($tag, 0, 1) == "#")
            {
                // Block of data, either a list or something that might not be shown
                if(substr($tag, 0, 2) == "##") continue; // This is the end tag
                $start = strpos($template, $original, 0);
                
                // find the end tag!
                $end_tag = str_replace("#", "##", $original);
                $end = strpos($template, $end_tag, $start);

                // Get the key to search for
                $data_key = str_replace("#", "", $tag);
                $replacement_text = "$original";
                if(array_key_exists($data_key, $data))
                {
                    $replacement_data = $data[$data_key];
                    if($replacement_data === false || empty($replacement_data))
                    {
                        $replacement_text = "";
                    }
                    else 
                    {            
                        $replacement_text = "";    
                        // what are we replacing it with
                        $inside_test = substr($template, $start + strlen($original) + 1, ($end - $start) - strlen($original) - 1);
                        foreach($replacement_data as $replacement_data_array)
                        {
                            $replacement_text .= self::stringReplace($inside_test, $replacement_data_array);
                        }
                    }
                }
                // replace the data
                $template = substr_replace($template, $replacement_text, $start, ($end - $start) + strlen($end_tag));
            }
            /*
            Add any other tags in here    
            elseif(substr($tag, 0, 1) == "#")
            {
                if(substr($tag, 0, 2) == "##") continue; // This is the end tag
            }
            */
            elseif(substr($tag, 0, 1) == "@")
            {
                // Conditional, only show if the data is matching
                // eg {{@test=test}} test {{@@test}}
                // Only shows if data[test] == test
                if(substr($tag, 0, 2) == "@@") continue; // This is the end tag
                
                $value = explode("=", $tag)[1];

                $start = strpos($template, $original, 0);                
                // find the end tag!
                $end_tag = "{{" . str_replace("@", "@@", explode("=", $tag)[0]) . "}}";
                $end = strpos($template, $end_tag, $start);

                $data_key = explode("=", str_replace("@", "", $tag))[0];
                $replacement_text = "$original";
                if(array_key_exists($data_key, $data))
                {
                    if($data[$data_key] == $value)
                    {
                        $replacement_text = substr($template, $start + strlen($original) + 1, ($end - $start) -  strlen($original) - 1);
                    }
                }
                // replace the data
                $template = substr_replace($template, $replacement_text, $start, ($end - $start) + strlen($end_tag));
            }
            else 
            {
                // Just a variable, replace the data
                if(array_key_exists($tag, $data))
                {
                    $template = str_replace($original, $data[$tag], $template);
                }    
            }
        }
        return $template;
    }

    
    private static function stringReplace($string, $data)
    {
        $returnString = $string;

        $regex = "/\{\{(.*?)\}\}/";
        preg_match_all($regex, $string, $items, PREG_OFFSET_CAPTURE);
        
        foreach($items[0] as $item)
        {        
            $original = $item[0];
            $tag = str_replace("{{", "", str_replace("}}", "", $original));
            
            $to_replace = "$tag";
            if(array_key_exists($tag, $data))
            {
                $to_replace = $data[$tag];
            }
            $returnString = str_replace($original, $to_replace, $returnString);
        }   

        return $returnString;
    }
}