<?php

class Request
{    
    public $url_elements = array();    
    public $parameters;    
    public $method;  
    public $controller_name;
    public $controller_reponse = "html";
    
    public static function FindController($name)
    {
        if(array_key_exists(strtolower($name), $GLOBALS["routing"]))
        {                
           return $GLOBALS["routing"][$name];
        }
    }

    public function __construct($url)
    {
        if($url != "")
        {
            $path_info = trim($url, "/") . "/";

            foreach(explode("/", $path_info) as $element)
            {
                if($element != "") array_push($this->url_elements, $element);
            }

            //Get Controller and Response
            $route_tmp = explode(".", strtolower($this->url_elements[0]));
            if(array_key_exists(strtolower($route_tmp[0]), $GLOBALS["routing"]))
            {                
                $this->controller_name = $GLOBALS["routing"][$route_tmp[0]];
            }
            else
            {
                // Try to Just use *Controller
                $this->controller_name = ucfirst($this->controller_name)."Controller";
            }
            // Get the response type
            if(isset($route_tmp[1])) $this->controller_reponse = $route_tmp[1];
        }
        else
        {
            // Default to home controller
            $this->controller_name = $GLOBALS["routing"]["home"];
        }


        //Get request method
        if(isset($this->url_elements[1])) $this->method = strtolower($this->url_elements[1]);
	    $this->url = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];

        $content = file_get_contents("php://input");
        if($content != "")  parse_str($content, $this->parameters);
        else $this->parameters = $_GET;
    }

}